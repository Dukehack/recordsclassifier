#ifndef DECLARATIONS_H
#define DECLARATIONS_H

#include "../RecordsClassifier/record.h"
#include "../RecordsClassifier/setofrules.h"

Q_DECLARE_METATYPE(QList<Record>);
Q_DECLARE_METATYPE(QList<SetOfRules>);

#endif
