#ifndef TESTCLASSIFIER_H
#define TESTCLASSIFIER_H

#include <QObject>
#include <QTest>
#include "../RecordsClassifier/classifier.h"
#include "declarations.h"
#include "testrunner.h"

typedef QList<SetOfRules> SetsOfRules;
typedef QList<Rule> Rules;
typedef QList<Record> Records;
typedef QSet<Property> Properties;
typedef QList<int> Numbers;
typedef QMap<QString, QStringList> Expectation;

class TestClassifier : public QObject
{
    Q_OBJECT

private slots:
    void testAnalyze_data();
    void testAnalyze();
};

DECLARE_TEST(TestClassifier);

#endif // TESTCLASSIFIER_H
