#include "testrecordsparser.h"

void TestRecordsParser::parse(const QString& text)
{
    xxcolumn = 1;
    xx_scan_string(text.toStdString().c_str());
    xxparse();
    xxlex_destroy();
}

void TestRecordsParser::cleanup()
{
    records.clear();
}

void TestRecordsParser::testYyparse_data()
{
    QTest::addColumn<QString>("text");
    QTest::addColumn<QList<Record>>("expected");

    typedef QList<Record> Records;
    typedef QSet<Property> Properties;
    typedef QList<int> Numbers;

    QTest::newRow("Empty array of numbers")
        << R"(@name
             property = {})"
        << Records({Record("name", Properties({Property("property", Numbers())}))});

    QTest::newRow("One record, one property")
        << R"(@name
             property = {1, 2, 3})"
        << Records({Record("name", Properties({Property("property", Numbers({1, 2, 3}))}))});

    QTest::newRow("One record, two properties")
        << R"(@name
             property1 = {1, 2, 3}
             property2 = {4, 5, 6})"
        << Records({Record("name", Properties({Property("property1", Numbers({1, 2, 3})), Property("property2", Numbers({4, 5, 6}))}))});

    QTest::newRow("Several records")
        << R"(@name1
             property1 = {1, 2, 3}
             property2 = {4, 5, 6}
             @name2
             property1 = {1, 2, 3}
             property2 = {4, 5, 6})"
        << Records({Record("name1", Properties({Property("property1", Numbers({1, 2, 3})), Property("property2", Numbers({4, 5, 6}))})), Record("name2", Properties({Property("property1", Numbers({1, 2, 3})), Property("property2", Numbers({4, 5, 6}))}))});
}

void TestRecordsParser::testYyparse()
{
    QFETCH(QString, text);
    QFETCH(QList<Record>, expected);

    parse(text);
    QVERIFY(records == expected);
}

void TestRecordsParser::testErrors_data()
{
    QTest::addColumn<QString>("text");

    QTest::newRow("There are not only digits and commas in array")
        << R"(@name prop = {^})";

    QTest::newRow("There are not only digits and commas in array")
        << R"(@name prop = {haha})";

    QTest::newRow("There are not only digits and commas in array")
        << R"(@name prop = {,})";

    QTest::newRow("There are not only digits and commas in array")
        << R"(@name prop = {@})";

    QTest::newRow("There are not only digits and commas in array")
        << R"(@name prop = {=})";

    QTest::newRow("There are not only digits and commas in array")
        << R"(@name prop = {{})";

    QTest::newRow("There are not only digits and commas in array")
        << R"(@name prop = {}})";

    QTest::newRow("There are not only digits and commas in array")
        << R"(@name prop = {1,(})";

    QTest::newRow("There are not only digits and commas in array")
        << R"(@name prop = {1,haha})";

    QTest::newRow("There are not only digits and commas in array")
        << R"(@name prop = {1,,})";

    QTest::newRow("There are not only digits and commas in array")
        << R"(@name prop = {1,@})";

    QTest::newRow("There are not only digits and commas in array")
        << R"(@name prop = {1,=})";

    QTest::newRow("There are not only digits and commas in array")
        << R"(@name prop = {1,{})";

    QTest::newRow("There are not only digits and commas in array")
        << R"(@name prop = {1,}})";

    QTest::newRow("There are not only digits and commas in array")
        << R"(@name prop = {1(})";

    QTest::newRow("There are not only digits and commas in array")
        << R"(@name prop = {1haha})";

    QTest::newRow("There are not only digits and commas in array")
        << R"(@name prop = {1,})";

    QTest::newRow("There are not only digits and commas in array")
        << R"(@name prop = {1@})";

    QTest::newRow("There are not only digits and commas in array")
        << R"(@name prop = {1=})";

    QTest::newRow("There are not only digits and commas in array")
        << R"(@name prop = {1{})";

    QTest::newRow("There are not only digits and commas in array")
        << R"(@name prop = {1}})";

    QTest::newRow("There are not only digits and commas in array")
        << R"(@name prop = {1,  2, 33, *})";
}

void TestRecordsParser::testErrors()
{
    QFETCH(QString, text);
    bool isExceptionWasCaught = false;

    try
    {
        parse(text);
    }
    catch (const QString &)
    {
        isExceptionWasCaught = true;
        xxlex_destroy();
    }

    if (isExceptionWasCaught)
    {
        QVERIFY(true == true);
    }
    else
    {
        QVERIFY(false == true);
    }
}
