#include "testinput.h"

void TestInput::cleanup()
{
    records.clear();
    setsOfRules.clear();
}

void TestInput::testParseRecords_data()
{
    QTest::addColumn<QString>("pathToFile");
    QTest::addColumn<QList<Record>>("expected");

    QTest::newRow("Empty array of numbers")
        << "tests/Records/EmptyArrayOfNumbers.txt"
        << Records({Record("name", Properties({Property("property", Numbers())}))});

    QTest::newRow("One record, one property")
        << "tests/Records/OneRecordOneProperty.txt"
        << Records({Record("name", Properties({Property("property", Numbers({1, 2, 3}))}))});

    QTest::newRow("One record, two properties")
        << "tests/Records/OneRecordTwoProperties.txt"
        << Records({Record("name", Properties({Property("property1", Numbers({1, 2, 3})), Property("property2", Numbers({4, 5, 6}))}))});

    QTest::newRow("Several records")
        << "tests/Records/SeveralRecords.txt"
        << Records({Record("name1", Properties({Property("property1", Numbers({1, 2, 3})), Property("property2", Numbers({4, 5, 6}))})), Record("name2", Properties({Property("property1", Numbers({1, 2, 3})), Property("property2", Numbers({4, 5, 6}))}))});
}

void TestInput::testParseRecords()
{
    QFETCH(QString, pathToFile);
    QFETCH(QList<Record>, expected);

    Classifier classifier;
    classifier.readFiles(pathToFile, pathToFile);
    Records localRecords = classifier.parseRecords();

    QVERIFY(localRecords == expected);
}

void TestInput::testParseRules_data()
{
    QTest::addColumn<QString>("pathToFile");
    QTest::addColumn<SetsOfRules>("expected");

    QTest::newRow("Empty array of numbers")
        << "tests/Rules/EmptyArrayOfNumbers.txt"
        << SetsOfRules({SetOfRules("name", Rules({Rule("rule", 1, 2, Numbers())}))});

    QTest::newRow("One set of rules, one rule")
        << "tests/Rules/OneSetOfRulesOneRule.txt"
        << SetsOfRules({SetOfRules("name", Rules({Rule("rule", 1, 2, Numbers({1, 2, 3}))}))});

    QTest::newRow("One set of rules, two rules")
        << "tests/Rules/OneSetOfRulesTwoRules.txt"
        << SetsOfRules({SetOfRules("name", Rules({Rule("rule1", 1, 2, Numbers({1, 2, 3})), Rule("rule2", 1, 2, Numbers({1, 2, 3}))}))});

    QTest::newRow("Several sets of rules")
        << "tests/Rules/SeveralSetsOfRules.txt"
        << SetsOfRules({SetOfRules("name1", Rules({Rule("rule1", 1, 2, Numbers({1, 2, 3})), Rule("rule2", 1, 2, Numbers({1, 2, 3}))})), SetOfRules("name2", Rules({Rule("rule1", 1, 2, Numbers({1, 2, 3})), Rule("rule2", 1, 2, Numbers({1, 2, 3}))}))});

    QTest::newRow("Set of rules without rules")
        << "tests/Rules/SetOfRulesWithoutRules.txt"
        << SetsOfRules({SetOfRules("name", Rules())});
}

void TestInput::testParseRules()
{
    QFETCH(QString, pathToFile);
    QFETCH(SetsOfRules, expected);

    Classifier classifier;
    classifier.readFiles(pathToFile, pathToFile);
    SetsOfRules localRules = classifier.parseSetsOfRules();

    QVERIFY(localRules == expected);
}
