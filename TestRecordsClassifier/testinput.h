#ifndef TESTINPUT_H
#define TESTINPUT_H

#include <QObject>
#include <QTest>
#include "../RecordsClassifier/classifier.h"
#include "testrunner.h"
#include "declarations.h"

typedef QList<Record> Records;
typedef QSet<Property> Properties;
typedef QList<int> Numbers;
typedef QList<SetOfRules> SetsOfRules;
typedef QList<Rule> Rules;

extern QList<Record> records;
extern QList<SetOfRules> setsOfRules;

class TestInput : public QObject
{
    Q_OBJECT

private slots:
    void cleanup();

    void testParseRecords_data();
    void testParseRecords();

    void testParseRules_data();
    void testParseRules();
};

DECLARE_TEST(TestInput);

#endif // TESTINPUT_H
