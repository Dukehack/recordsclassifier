#include "testclassifier.h"

void TestClassifier::testAnalyze_data()
{
    QTest::addColumn<Records>("records");
    QTest::addColumn<SetsOfRules>("setsOfRules");
    QTest::addColumn<Expectation>("expectation");

    QTest::newRow("No matches")
        << Records({Record("record", Properties({Property("property", Numbers({1, 2, 3}))}))})
        << SetsOfRules({SetOfRules("class", Rules({Rule("rule", 1, 0, Numbers())}))})
        << Expectation();

    QTest::newRow("Rule 1")
        << Records({Record("record", Properties({Property("property", Numbers({1, 2, 3}))}))})
        << SetsOfRules({SetOfRules("class", Rules({Rule("property", 1, 0, Numbers())}))})
        << Expectation{{"record", QStringList({"class"})}};

    QTest::newRow("Rule 1, two classes match one record")
        << Records({Record("record", Properties({Property("property", Numbers({1, 2, 3}))}))})
        << SetsOfRules({SetOfRules("class1", Rules({Rule("property", 1, 0, Numbers())})), SetOfRules("class2", Rules({Rule("property", 1, 0, Numbers())}))})
        << Expectation{{"record", QStringList({"class1", "class2"})}};

    QTest::newRow("Rule 1, two classes, two records, each class matches one record")
        << Records({Record("record1", Properties({Property("property1", Numbers({1, 2, 3}))})), Record("record2", Properties({Property("property2", Numbers({1, 2, 3}))}))})
        << SetsOfRules({SetOfRules("class1", Rules({Rule("property1", 1, 0, Numbers())})), SetOfRules("class2", Rules({Rule("property2", 1, 0, Numbers())}))})
        << Expectation{{"record1", QStringList({"class1"})}, {"record2", QStringList({"class2"})}};

    QTest::newRow("Rule 2")
        << Records({Record("record", Properties({Property("property", Numbers({1, 2, 3}))}))})
        << SetsOfRules({SetOfRules("class", Rules({Rule("property", 2, 3, Numbers({1, 2, 3}))}))})
        << Expectation{{"record", QStringList({"class"})}};

    QTest::newRow("Rule 3")
        << Records({Record("record", Properties({Property("property", Numbers({1, 2, 3}))}))})
        << SetsOfRules({SetOfRules("class", Rules({Rule("property", 3, 3, Numbers({1, 2, 3}))}))})
        << Expectation{{"record", QStringList({"class"})}};

    QTest::newRow("Rule 4")
        << Records({Record("record", Properties({Property("property", Numbers({1, 2, 3}))}))})
        << SetsOfRules({SetOfRules("class", Rules({Rule("property", 4, 3, Numbers({1, 2, 3}))}))})
        << Expectation{{"record", QStringList({"class"})}};

    QTest::newRow("Empty set of rules must match any record")
        << Records({Record("record", Properties({Property("property", Numbers({1, 2, 3}))}))})
        << SetsOfRules({SetOfRules("class", Rules())})
        << Expectation{{"record", QStringList({"class"})}};

    QTest::newRow("Complicated cases - two different classes with different rules match one record")
        << Records({Record("record", Properties({Property("property", Numbers({1, 2, 3}))}))})
        << SetsOfRules({SetOfRules("class1", Rules({Rule("property", 1, 0, Numbers())})), SetOfRules("class2", Rules({Rule("property", 2, 3, Numbers())}))})
        << Expectation{{"record", QStringList({"class1", "class2"})}};

    QTest::newRow("Complicated cases - classes A and B match record1, and classes C and B match record2")
        << Records({Record("record1", Properties({Property("property1", Numbers({1, 2, 3}))})), Record("record2", Properties({Property("property2", Numbers({1}))}))})
        << SetsOfRules({SetOfRules("A", Rules({Rule("property1", 1, 0, Numbers())})), SetOfRules("B", Rules({Rule("property1", 2, 3, Numbers()), Rule("property2", 1, 3, Numbers())})), SetOfRules("C", Rules({Rule("property2", 1, 0, Numbers())}))})
        << Expectation{{"record1", QStringList({"A", "B"})}, {"record2", QStringList({"B", "C"})}};
}

void TestClassifier::testAnalyze()
{
    QFETCH(Records, records);
    QFETCH(SetsOfRules, setsOfRules);
    QFETCH(Expectation, expectation);

    Expectation result = Classifier().classify(records, setsOfRules);
    QVERIFY(result == expectation);
}
