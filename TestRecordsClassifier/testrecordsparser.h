#ifndef TESTRECORDSPARSER_H
#define TESTRECORDSPARSER_H

#include <QObject>
#include <QTest>
#include <QList>
#include "../RecordsClassifier/recordslexer.hpp"
#include "../RecordsClassifier/recordsparser.hpp"
#include "../RecordsClassifier/record.h"
#include "declarations.h"
#include "testrunner.h"

extern int xxcolumn;
extern QList<Record> records;

class TestRecordsParser : public QObject
{
    Q_OBJECT

private:
    void parse(const QString &text);

private slots:
    void cleanup();

    void testYyparse_data();
    void testYyparse();

    void testErrors_data();
    void testErrors();
};

DECLARE_TEST(TestRecordsParser);

#endif // TESTRECORDSPARSER_H
