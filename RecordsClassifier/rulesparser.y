%defines
%locations
%error-verbose
%define api.prefix {zz}

%code requires
{
    #include <QString>
    #include "setofrules.h"
}

%{
    #pragma warning(disable: 4996)
    #include <QString>
    #include <QList>
    #include "setofrules.h"

    QList<SetOfRules> setsOfRules;
    QString zzerrorMessage = "Array contains foreign symbols";

    extern int zzlex();
    extern void zzerror(const char *msg);
%}

%union
{
    int intVal;
    QString *strVal;
    Rule *rule;
    QList<Rule> *rules;
    SetOfRules *setOfRule;
    QList<int> *numbers;
}

%token<intVal> ZZNUMBER
%token<strVal> ZZID
%token ZZSTRANGE_SYMBOL

%type<rule> Rule
%type<rules> Rules
%type<setOfRule> SetOfRules
%type<numbers> Numbers

%start SetsOfRules

%%
SetsOfRules
    : SetOfRules {setsOfRules << *$1; delete $1;}
    | SetsOfRules SetOfRules {setsOfRules << *$2; delete $2;}
    ;

SetOfRules
    : '@' ZZID Rules {$$ = new SetOfRules(*$2, *$3); delete $2; delete $3;}
    | '@' ZZID {$$ = new SetOfRules(*$2, QList<Rule>()); delete $2;}
    ;

Rules
    : Rule {$$ = new QList<Rule>(); *$$ << *$1; delete $1;}
    | Rules Rule {*$$ << *$2; delete $2;}
    ;

Rule
    : ZZID '=' ZZNUMBER ZZNUMBER '{' Numbers '}' {$$ = new Rule(*$1, $3, $4, *$6); delete $1; delete $6;}
    | ZZID '=' ZZNUMBER ZZNUMBER '{' '}' {$$ = new Rule(*$1, $3, $4, QList<int>()); delete $1;}
    | ZZID '=' ZZNUMBER ZZNUMBER '{' ZZSTRANGE_SYMBOL '}' {delete $1; throw zzerrorMessage;}
    | ZZID '=' ZZNUMBER ZZNUMBER '{' ZZID '}' {delete $1; throw zzerrorMessage;}
    | ZZID '=' ZZNUMBER ZZNUMBER '{' ',' '}' {delete $1; throw zzerrorMessage;}
    | ZZID '=' ZZNUMBER ZZNUMBER '{' '@' '}' {delete $1; throw zzerrorMessage;}
    | ZZID '=' ZZNUMBER ZZNUMBER '{' '=' '}' {delete $1; throw zzerrorMessage;}
    | ZZID '=' ZZNUMBER ZZNUMBER '{' '{' '}' {delete $1; throw zzerrorMessage;}
    | ZZID '=' ZZNUMBER ZZNUMBER '{' '}' '}' {delete $1; throw zzerrorMessage;}
    ;

Numbers
    : ZZNUMBER {$$ = new QList<int>(); *$$ << $1;}
    | Numbers ',' ZZNUMBER {*$$ << $3;}
    | Numbers ',' ZZSTRANGE_SYMBOL {delete $$; throw zzerrorMessage;}
    | Numbers ',' ZZID {delete $$; throw zzerrorMessage;}
    | Numbers ',' ',' {delete $$; throw zzerrorMessage;}
    | Numbers ',' '@' {delete $$; throw zzerrorMessage;}
    | Numbers ',' '=' {delete $$; throw zzerrorMessage;}
    | Numbers ',' '{' {delete $$; throw zzerrorMessage;}
    | Numbers ',' '}' {delete $$; throw zzerrorMessage;}
    | Numbers ',' '}' {delete $$; throw zzerrorMessage;}
    | Numbers ZZSTRANGE_SYMBOL {delete $$; throw zzerrorMessage;}
    | Numbers ZZID {delete $$; throw zzerrorMessage;}
    | Numbers ',' {delete $$; throw zzerrorMessage;}
    | Numbers '@' {delete $$; throw zzerrorMessage;}
    | Numbers '=' {delete $$; throw zzerrorMessage;}
    | Numbers '{' {delete $$; throw zzerrorMessage;}
    | Numbers '}' {delete $$; throw zzerrorMessage;}
    ;
%%
