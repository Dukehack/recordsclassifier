#include <QString>
#include "classifier.h"

Classifier::Classifier()
{
}

Classifier::~Classifier()
{
}

void Classifier::readFiles(const QString &recordsFilePath, const QString &rulesFilePath)
{
    // Read file with records.
    QFile recordsFile(recordsFilePath);
    if (!recordsFile.open(QIODevice::ReadOnly))
    {
        throw QString("Couldn`t open file ") + recordsFilePath;
    }

    QTextStream input1(&recordsFile);
    recordsFileContent = input1.readAll();

    // Read file with rules.
    QFile rulesFile(rulesFilePath);
    if (!rulesFile.open(QIODevice::ReadOnly))
    {
        throw QString("Couldn`t open file ") + rulesFilePath;
    }

    QTextStream input2(&rulesFile);
    rulesFileContent = input2.readAll();

    this->recordsFilePath = recordsFilePath;
    this->rulesFilePath = rulesFilePath;
}

Records Classifier::parseRecords() throw (const QString &)
{
    xx_scan_string(recordsFileContent.toStdString().c_str());
    int parseResult = xxparse();
    // Flush lex merory.
    xxlex_destroy();

    switch (parseResult)
    {
        case 0: break;
        case 1: throw QString("There are syntax errors");
            break;
        case 2: throw QString("Bison: error while freeing memory");
    }

    // There are non fatal errors.
    if (xxnerrs)
    {
        throw QString("Couldn`t parse file ") + recordsFilePath;
    }

    return records;
}

SetsOfRules Classifier::parseSetsOfRules() throw (const QString &)
{
    zz_scan_string(rulesFileContent.toStdString().c_str());
    int parseResult = zzparse();
    // Flush lex merory.
    zzlex_destroy();

    switch (parseResult)
    {
        case 0: break;
        case 1: throw QString("There are syntax errors");
            break;
        case 2: throw QString("Bison: error while freeing memory");
    }

    // There are non fatal errors.
    if (zznerrs)
    {
        throw QString("Couldn`t parse file ") + rulesFilePath;
    }

    return setsOfRules;
}

RecordsToClassesMap Classifier::classify(const Records &records, const SetsOfRules &setsOfRules)
{
    QMap<QString, QStringList> result;
    QStringList emptySetsOfRules;

    for (auto setOfRules : setsOfRules)
    {
        // Set of rules is empty.
        if (setOfRules.getRules().length() == 0)
        {
            emptySetsOfRules.append(setOfRules.getName());
        }

        for (auto rule : setOfRules.getRules())
        {
            for (auto record : records)
            {
                if (record.getProperties().contains(Property(rule.getName())))
                {
                    QSet<Property> properties = record.getProperties();
                    QSet<Property>::iterator iter = properties.find(Property(rule.getName()));
                    Property property = *iter;

                    switch (rule.getId())
                    {
                        // Rule 1. Name of record property must equals name of rule.
                        case 1:
                            addValueToMap(result, record.getName(), setOfRules.getName());
                            break;
                            // Rule 2. Name of record property must equals name of rule and number of rule must equals property array of numbers length.
                        case 2:
                            if (rule.getNumber() == property.getNumbers().length())
                            {
                                addValueToMap(result, record.getName(), setOfRules.getName());
                            }
                            break;
                            // Rule 3. Name of record property must equals name of rule and property array of numbers must contain rule number.
                        case 3:
                            if (property.getNumbers().contains(rule.getNumber()))
                            {
                                addValueToMap(result, record.getName(), setOfRules.getName());
                            }
                            break;
                            // Rule 4. Name of record property must equals name of rule and arrays must be equal.
                        case 4:
                            if (rule.getNumbers() == property.getNumbers())
                            {
                                addValueToMap(result, record.getName(), setOfRules.getName());
                            }
                            break;
                        default:
                            throw QString("Id must be in [1; 4]");
                    }
                }
            }
        }
    }

    addEmptySetsOfRulesToAllRecords(result, emptySetsOfRules, records);

    return result;
}

void Classifier::writeResultToFile(const RecordsToClassesMap &recordsToClassesMap)
{
    QFile file("result.txt");
    if (!file.open(QIODevice::WriteOnly))
    {
        throw QString("Cannot create file for result output");
    }

    QTextStream out(&file);
    foreach(QString key, recordsToClassesMap.keys())
    {
        auto setsOfRules = recordsToClassesMap.value(key);
        // Output result in format: nameOfRecord = nameOfClass1, ..., nameOfClassN
        out << key << " = " << setsOfRules.join(", ") << endl;
    }
}

void Classifier::addValueToMap(QMap<QString, QStringList> &map, const QString &key, const QString &value)
{
    if (map.contains(key))
    {
        map[key].append(value);
    }
    else
    {
        map.insert(key, QStringList(value));
    }
}

void Classifier::addEmptySetsOfRulesToAllRecords(RecordsToClassesMap &map, const QStringList &emptySetsOfRules, const Records &records)
{
    for (auto nameOfSetOfEmptyRule : emptySetsOfRules)
    {
        for (auto record : records)
        {
            addValueToMap(map, record.getName(), nameOfSetOfEmptyRule);
        }
    }
}
