#include "classifier.h"

extern char *xxfilename;
extern char *zzfilename;

int main(int argc, char *argv[])
{
    try
    {
        if (argc != 3)
        {
            QTextStream out(stderr);
            out << QString("Invalid number of arguments") << endl;
            exit(255);
        }

        xxfilename = argv[1];
        zzfilename = argv[2];

        Classifier classifier;
        classifier.readFiles(argv[1], argv[2]);
        Records records = classifier.parseRecords();
        SetsOfRules setsOfRules = classifier.parseSetsOfRules();
        classifier.writeResultToFile(classifier.classify(records, setsOfRules));
    }
    catch (const QString &e)
    {
        QTextStream out(stderr);
        out << e << endl;
        exit(255);
    }

    return 0;
}
