#include "record.h"

Record::Record(const QString &name, const QSet<Property> &properties)
{
    this->name = name;
    this->properties = properties;
}

Record::~Record()
{
}

bool Record::operator==(const Record &record)
{
    return
        this->name == record.name &&
        this->properties == record.properties;
}
