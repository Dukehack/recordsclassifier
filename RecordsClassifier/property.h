/*!
 *\file
 * Property class.
 */

#ifndef PROPERTY_H
#define PROPERTY_H

#include <QString>
#include <QList>
#include <QHash>

/*!
 * Property class.
 */
class Property
{
public:
    /*! Construct an instance of class. */
    Property(const QString &name, const QList<int> &values);
    Property(const QString &name);
    ~Property();

    /*! Compare operator. */
    bool operator==(const Property &property);

    QString getName() const
    {
        return name;
    }

    QList<int> getNumbers() const
    {
        return numbers;
    }

private:
    QString name;
    QList<int> numbers;
};

inline bool operator==(const Property &first, const Property &second)
{
    return first.getName() == second.getName();
}

inline uint qHash(const Property &propety)
{
    return qHash(propety.getName());
}

#endif // PROPERTY_H
