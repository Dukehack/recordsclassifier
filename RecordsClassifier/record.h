/*!
 *\file
 * Record class.
 */

#ifndef RECORD_H
#define RECORD_H

#include <QString>
#include <QSet>
#include "property.h"

/*!
 * Record class.
 */
class Record
{
public:
    /*! Construct an instance of class. */
    Record(const QString &name, const QSet<Property> &properties);
    ~Record();

    /*! Compare operator. */
    bool operator==(const Record &record);

    QString getName() const
    {
        return name;
    }

    QSet<Property> getProperties() const
    {
        return properties;
    }

private:
    QString name;
    QSet<Property> properties;
};

#endif // RECORD_H
