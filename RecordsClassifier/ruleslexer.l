%option noyywrap never-interactive outfile="ruleslexer.cpp" header-file="ruleslexer.hpp" prefix="zz"

%top {
    #pragma warning(disable: 4996)
}

%{
    #include "unistd.h"
    #include "rulesparser.hpp"

    int zzcolumn = 1;
    char *zzfilename;
    int myzzlineno = 1;

    #define YY_USER_ACTION \
        zzlloc.first_line = zzlloc.last_line = myzzlineno; \
        zzlloc.first_column = zzcolumn; \
        zzlloc.last_column = zzcolumn + zzleng - 1; \
        zzcolumn += zzleng;

    void zzerror(const char *msg)
    {
        printf("%s:%d.%d: %s\n", zzfilename, zzlloc.first_line, zzlloc.first_column, msg);
    }
%}

digit [0-9]
number {digit}+

letter [a-zA-Z_]
id {letter}({letter}|{digit})*

cr [\r]
lf [\n]
eol ({cr}{lf}|{lf}|{cr})

space [ ]
tab [\t]
spaces ({space}|{tab})

%%
"," {return zztext[0];}
"=" {return zztext[0];}
"{" {return zztext[0];}
"}" {return zztext[0];}
"@" {return zztext[0];}

{spaces} {}
{eol} {zzcolumn = 1; myzzlineno++;}
{number} {zzlval.intVal = atoi(zztext); return ZZNUMBER;}
{id} {zzlval.strVal = new QString(zztext); return ZZID;}

. {zzcolumn++; return ZZSTRANGE_SYMBOL;}
%%
