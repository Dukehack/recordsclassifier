%defines
%locations
%error-verbose
%define api.prefix {xx}

%code requires
{
    #include <QString>
    #include "record.h"
}

%{
    #pragma warning(disable: 4996)
    #include <QString>
    #include <QList>
    #include <QSet>
    #include "record.h"

    QList<Record> records;
    QString xxerrorMessage = "Array contains foreign symbols";

    extern int xxlex();
    extern void xxerror(const char *msg);
%}

%union
{
    int intVal;
    QString *strVal;
    Record *record;
    Property *property;
    QSet<Property> *properties;
    QList<int> *numbers;
}

%token<intVal> XXNUMBER
%token<strVal> XXID
%token XXSTRANGE_SYMBOL

%type<property> Property
%type<properties> Properties
%type<record> Record
%type<numbers> Numbers

%start Records

%%
Records
    : Record {records << *$1; delete $1;}
    | Records Record {records << *$2; delete $2;}
    ;

Record
    : '@' XXID Properties {$$ = new Record(*$2, *$3); delete $2; delete $3;}
    ;

Properties
    : Property {$$ = new QSet<Property>(); *$$ << *$1; delete $1;}
    | Properties Property {*$$ << *$2; delete $2;}
    ;

Property
    : XXID '=' '{' Numbers '}' {$$ = new Property(*$1, *$4); delete $1; delete $4;}
    | XXID '=' '{' '}' {$$ = new Property(*$1, QList<int>()); delete $1;}
    | XXID '=' '{' XXSTRANGE_SYMBOL '}' {delete $1; throw xxerrorMessage;}
    | XXID '=' '{' XXID '}' {delete $1; delete $4; throw xxerrorMessage;}
    | XXID '=' '{' ',' '}' {delete $1; throw xxerrorMessage;}
    | XXID '=' '{' '@' '}' {delete $1; throw xxerrorMessage;}
    | XXID '=' '{' '=' '}' {delete $1; throw xxerrorMessage;}
    | XXID '=' '{' '{' '}' {delete $1; throw xxerrorMessage;}
    | XXID '=' '{' '}' '}' {delete $1; throw xxerrorMessage;}
    ;

Numbers
    : XXNUMBER {$$ = new QList<int>(); *$$ << $1;}
    | Numbers ',' XXNUMBER {*$$ << $3;}
    | Numbers ',' XXSTRANGE_SYMBOL {delete $$; throw xxerrorMessage;}
    | Numbers ',' XXID {delete $$; throw xxerrorMessage;}
    | Numbers ',' ',' {delete $$; throw xxerrorMessage;}
    | Numbers ',' '@' {delete $$; throw xxerrorMessage;}
    | Numbers ',' '=' {delete $$; throw xxerrorMessage;}
    | Numbers ',' '{' {delete $$; throw xxerrorMessage;}
    | Numbers ',' '}' {delete $$; throw xxerrorMessage;}
    | Numbers ',' '}' {delete $$; throw xxerrorMessage;}
    | Numbers XXSTRANGE_SYMBOL {delete $$; throw xxerrorMessage;}
    | Numbers XXID {delete $$; throw xxerrorMessage;}
    | Numbers ',' {delete $$; throw xxerrorMessage;}
    | Numbers '@' {delete $$; throw xxerrorMessage;}
    | Numbers '=' {delete $$; throw xxerrorMessage;}
    | Numbers '{' {delete $$; throw xxerrorMessage;}
    | Numbers '}' {delete $$; throw xxerrorMessage;}
    ;
%%
