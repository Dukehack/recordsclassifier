/* A Bison parser, made by GNU Bison 3.0.4.  */

/* Bison interface for Yacc-like parsers in C

   Copyright (C) 1984, 1989-1990, 2000-2015 Free Software Foundation, Inc.

   This program is free software: you can redistribute it and/or modify
   it under the terms of the GNU General Public License as published by
   the Free Software Foundation, either version 3 of the License, or
   (at your option) any later version.

   This program is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
   GNU General Public License for more details.

   You should have received a copy of the GNU General Public License
   along with this program.  If not, see <http://www.gnu.org/licenses/>.  */

/* As a special exception, you may create a larger work that contains
   part or all of the Bison parser skeleton and distribute that work
   under terms of your choice, so long as that work isn't itself a
   parser generator using the skeleton or a modified version thereof
   as a parser skeleton.  Alternatively, if you modify or redistribute
   the parser skeleton itself, you may (at your option) remove this
   special exception, which will cause the skeleton and the resulting
   Bison output files to be licensed under the GNU General Public
   License without this special exception.

   This special exception was added by the Free Software Foundation in
   version 2.2 of Bison.  */

#ifndef YY_XX_RECORDSPARSER_HPP_INCLUDED
# define YY_XX_RECORDSPARSER_HPP_INCLUDED
/* Debug traces.  */
#ifndef XXDEBUG
# if defined YYDEBUG
#if YYDEBUG
#   define XXDEBUG 1
#  else
#   define XXDEBUG 0
#  endif
# else /* ! defined YYDEBUG */
#  define XXDEBUG 0
# endif /* ! defined YYDEBUG */
#endif  /* ! defined XXDEBUG */
#if XXDEBUG
extern int xxdebug;
#endif
/* "%code requires" blocks.  */
#line 7 "recordsparser.y" /* yacc.c:1909  */

    #include <QString>
    #include "record.h"

#line 57 "recordsparser.hpp" /* yacc.c:1909  */

/* Token type.  */
#ifndef XXTOKENTYPE
# define XXTOKENTYPE
  enum xxtokentype
  {
    XXNUMBER = 258,
    XXID = 259,
    XXSTRANGE_SYMBOL = 260
  };
#endif

/* Value type.  */
#if ! defined XXSTYPE && ! defined XXSTYPE_IS_DECLARED

union XXSTYPE
{
#line 27 "recordsparser.y" /* yacc.c:1909  */

    int intVal;
    QString *strVal;
    Record *record;
    Property *property;
    QSet<Property> *properties;
    QList<int> *numbers;

#line 84 "recordsparser.hpp" /* yacc.c:1909  */
};

typedef union XXSTYPE XXSTYPE;
# define XXSTYPE_IS_TRIVIAL 1
# define XXSTYPE_IS_DECLARED 1
#endif

/* Location type.  */
#if ! defined XXLTYPE && ! defined XXLTYPE_IS_DECLARED
typedef struct XXLTYPE XXLTYPE;
struct XXLTYPE
{
  int first_line;
  int first_column;
  int last_line;
  int last_column;
};
# define XXLTYPE_IS_DECLARED 1
# define XXLTYPE_IS_TRIVIAL 1
#endif


extern XXSTYPE xxlval;
extern XXLTYPE xxlloc;
int xxparse (void);

#endif /* !YY_XX_RECORDSPARSER_HPP_INCLUDED  */
