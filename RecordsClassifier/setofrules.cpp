#include "setofrules.h"

SetOfRules::SetOfRules(const QString &name, const QList<Rule> &rules)
{
    this->name = name;
    this->rules = rules;
}

SetOfRules::~SetOfRules()
{
}

bool SetOfRules::operator==(const SetOfRules &setOfRules)
{
    return
        this->name == setOfRules.name &&
        this->rules == setOfRules.rules;
}
