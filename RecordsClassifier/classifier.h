/*!
 *\file
 * Classifier class. Classifies records.
 */

#ifndef CLASSIFIER_H
#define CLASSIFIER_H

#include <QMap>
#include <QStringList>
#include <QTextStream>
#include <QString>
#include <QFile>
#include <QTextStream>
#include "recordslexer.hpp"
#include "recordsparser.hpp"
#include "ruleslexer.hpp"
#include "rulesparser.hpp"
#include "record.h"
#include "setofrules.h"

typedef QList<Record> Records;
typedef QList<SetOfRules> SetsOfRules;
typedef QMap<QString, QStringList> RecordsToClassesMap;

extern int xxnerrs;
extern int zznerrs;
extern Records records;
extern SetsOfRules setsOfRules;

/*!
 * Classifier class.
 */
class Classifier
{
public:
    Classifier();
    ~Classifier();

    /*! Reads files.
     *\param[in] recordsFilePath Path where file with records is located.
     *\param[in] rulesFilePath Path where file with rules is located.
     *\throw QString If it impossible to read from files.
     */
    void readFiles(const QString &recordsFilePath, const QString &rulesFilePath) throw (const QString &);

    /*! Fills records.
     *\throw QString If there are syntax errors in records file.
     */
    Records parseRecords() throw (const QString &);

    /*! Fills sets of rules.
     *\throw QString If there are syntax errors in records file.
     */
    SetsOfRules parseSetsOfRules() throw (const QString &);

    /*! Classifies records.
     *\param[in] records List of records.
     *\param[in] setsOfRules List of sets of rules.
     *\throw QString Id is in not the range [1; 4].
     */
    RecordsToClassesMap classify(const Records &records, const SetsOfRules &setsOfRules) throw (const QString &);

    /*! Writes result to file.
     *\param[in] recordsToClassesMap Map where key is name of record and value is list of set of rule names.
     *\throw QString It is impossible to write to file.
     */
    void writeResultToFile(const RecordsToClassesMap &recordsToClassesMap) throw (const QString &);

private:
    /*! Adds set of rules name to corresponding record name.
     *\param[in|out] map A place where to add.
     *\param[in] key Name of record.
     *\param[in] value Name of set of rule.
     */
    void addValueToMap(QMap<QString, QStringList> &map, const QString &key, const QString &value);

    /*! Adds empy sets of rules to all records.
     *\param[in|out] map A place where to add.
     *\param[in] emptySetsOfRules Sets of rules which are empty.
     *\param[in] records List of records.
     */
    void addEmptySetsOfRulesToAllRecords(RecordsToClassesMap &map, const QStringList &emptySetsOfRules, const Records &records);

    QString recordsFileContent;
    QString rulesFileContent;
    QString recordsFilePath;
    QString rulesFilePath;
};

#endif // CLASSIFIER_H
